# Rust-course #

Rust tutorial for beginner

### Want to know more? ###

Rust Language source
https://github.com/rust-lang/rust

Install rustup from:
curl https://sh.rustup.rs -sSf | sh

RUST findings:

Parity project:

Rust VisualStudio IDE setup:
http://asquera.de/blog/2017-03-03/setting-up-a-rust-devenv/

Crates.io is the package repository for the Rust

https://locka99.gitbooks.io/a-guide-to-porting-c-to-rust/features_of_rust/

https://medium.com/@marekkotewicz/building-a-mobile-app-in-rust-and-react-native-part-1-project-setup-b8dbcf3f539f

https://www.rust-lang.org/en-US/community.html

Debugging rust with GDB
http://thornydev.blogspot.co.uk/2014/01/debugging-rust-with-gdb.html

Rust by example
https://rustbyexample.com/primitives.html


https://klausi.github.io/rustnish/2017/05/28/using-visual-studio-code-for-rust-on-ubuntu.html

https://doc.rust-lang.org/book/first-edition/lifetimes.html

