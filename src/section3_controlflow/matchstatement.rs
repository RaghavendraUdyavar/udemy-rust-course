//#![allow()]

pub fn matchstatement_function() {
    println!("I am matchstatement");

    let country_code = 1400;

    let country = match country_code {
        44 => "UK",
        46 => "Sweden",
        91 => "India",
        65 => "Singapore",
        1...999  => "Unknown",
        _ => "Invalid"
    };

    println!("Country code with {} is {}", country_code, country);
}