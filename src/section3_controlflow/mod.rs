#![allow(dead_code)]

mod controlflow;
mod whileflow;
mod forloopflow;

pub mod matchstatement;

pub fn controlflowfunction(){
    controlflow::controlflowfn();
}

pub fn whileloopfunction() {
    whileflow::while_loop();
}