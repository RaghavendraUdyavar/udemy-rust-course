#![allow(dead_code)]

pub fn for_loop() {
    for m in 4..21 {
        println!("m : {}", m);
    }

    for (pos, n) in (30..41).enumerate() {
        println!("{} : {}", pos, n);
    }
}