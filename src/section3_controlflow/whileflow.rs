#![allow(dead_code)]
#![allow(unused_imports)]

use section3_controlflow::forloopflow;

pub fn while_loop() {
    let mut x = 1;
    loop {
        x = x << 1;
        if x > 1000 {
            break
        }
        println!("x : {}", x);
    }
}
