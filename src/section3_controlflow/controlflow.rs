pub fn controlflowfn(){
    println!("I am currently in control flow");

    let temp = 30;
    if temp > 30 {
        println!("temperature is hot");
    } else if temp < 30 {
        println!("temperature is cold");
    } else {
        println!("temperature is OK");
    }

    let day = if temp > 20 { "sunny" } else {"cloudy"};
    println!("day is {}", day);

}
