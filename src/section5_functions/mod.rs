#![allow(dead_code)]

mod functions;
mod methods;

pub fn call_functions() {
    functions::callfunctions();
}

pub fn call_methods() {
    methods::callmethods();
}