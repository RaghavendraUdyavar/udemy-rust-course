pub fn callfunctions() {
    print_value(5);
}

fn print_value(x: i32) {
    println!("value = {}", x);

    let mut increasex = x;
    increase(&mut increasex);
    println!("increased value = {}", increasex);

    let productx = product(x, increasex);
    println!("product of {} * {} = {}", x, increasex, productx);
}

fn product(x: i32, y: i32) -> i32 {
    x * y
} 

fn increase (x : &mut i32){
    *x += 1;
}