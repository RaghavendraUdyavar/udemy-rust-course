#![allow(unused_imports)]
mod section3_controlflow;
mod section4_datastructure;
mod section5_functions;

use section3_controlflow::matchstatement;

fn main() {
    //println!("Hello, world!");

    //section3::controlflowfunction();
    //section3::whileloopfunction();
    //matchstatement::matchstatement_function();

    //section4_datastructure::call_createstructure();
    //section4_datastructure::call_createenums();
    //section4_datastructure::call_createunions();
    //section4_datastructure::call_createoptions();
    //section4_datastructure::call_createarrays();
    //section4_datastructure::call_createvectors();
    //section4_datastructure::call_createslices();
    //section4_datastructure::call_createstrings();
    //section4_datastructure::call_createtuples();
    //section4_datastructure::call_patternmatching();
    //section5_functions::call_functions();
    section5_functions::call_methods();
}