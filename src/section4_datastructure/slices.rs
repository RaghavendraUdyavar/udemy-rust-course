pub fn slices() {
    let data = [1, 2, 3, 4, 5];
    use_slice(&data[1..4]);
}

fn use_slice(slice: &[i32]){
    for value in slice {
        println!("{}", value);
    }
}