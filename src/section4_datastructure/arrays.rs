pub fn createarrays()
{
    let a = [0, 3, 4, 5, 2];
    let n = 2;
    println!("length of array a : {}, and the element in a[{}] is {}", 
    a.len(), n, a[n]);

    //println!("The 10th element is {}", a[10]);    
    println!("The entire array {:?}", a);

    createmultidimensionalarray();
}

fn createmultidimensionalarray() {
    let b:[[f64;3]; 2] = [[100.22837499129389234234234, 1.0, 0.0], [2.0, 1.0, 3.0]];

    for i in 0..b.len() {
        for j in 0..b[i].len() {
            print!("value at {}, {} : {}", i, j, b[i][j]);
        }
    }
}