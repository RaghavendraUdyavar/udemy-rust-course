pub fn createstrings(){
    let mut s:&'static str = "hello! there";

    for c in s.chars().rev() {
        println!("{}", c);
    }

    s = "what is out there with you";

    if let Some(first_char) = s.chars().rev().nth(0) {
        println!("{}", first_char);
    }

    let mut letters = String::new();
    let mut a = 'a' as u8;
    while a <= 'z' as u8 {
        letters.push(a as char);
        letters.push_str(",");

        a += 1;
    }

    println!("{}", letters);
}
