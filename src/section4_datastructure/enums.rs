enum Color {
    Red,
    Green,
    Blue,
    RGBColor(u8, u8, u8),
    CMYK { cyan:u8, magenta:u8, yellow:u8, black:u8 }
}

pub fn createenums() {
    let c:Color = Color::CMYK { cyan:5, magenta: 0, yellow: 0, black: 0};

    match c {
        Color::Red => println!("Red Color"),
        Color::Blue => println!("Blue Color"),
        Color::RGBColor(255, 0, 0) => println!("Tuple Red Color"),
        Color::RGBColor(r, g, b) => println!("rgb({}, {}, {})", r,g,b),
        Color::CMYK{cyan:c, magenta:m, yellow:y, black:k} => 
        println!("CMYK {{ {}, {}, {}, {} }}", c, m, y, k),
        _ => println!("Color not defined")
    }
}