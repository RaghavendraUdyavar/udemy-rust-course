#[derive(Debug)]
pub struct Point {
    x: f64,
    y: f64
}

pub struct Line {
    start : Point,
    end :Point
}

pub fn createstructure(){
    let p = Point { x: 3.0, y: 2.0 };
    println!("p is: ({:?})" , p);
}

impl Point {
    fn sum_and_product(&self) -> (f64, f64) {
        (self.x + self.y, self.x * self.y)
    }
}