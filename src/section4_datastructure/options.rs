pub fn createoptions() {
    let x = 3.0;
    let y = 2.0;

    let result : Option<f64> = if y != 0.0 { Some(x/y) } else { None };
    println!("result: {:?}", result);

    if y != 0.0 {
        println!("Testinng if statement here");
    } else {
        println!(" Ok I kknow that Testinng if statement here");
    }

    match result {
        Some(z) => println!("{}/{} = {}", x, y, z),
        None => println!("cannot divide by {}", y)
    }

    if let Some(z) = result { println!("z :{:?}", z) }
}