pub fn createvectors() {
    let mut vec:Vec<u32> = Vec::new();
    vec.push(10);
    vec.push(2);
    vec.push(56);
    vec.push(277);
    vec.push(543);

    let idx = 10;
    
    match vec.get(idx) {
        Some(a) => println!("value at vec[{}] = {}", idx, a),
        None => println!("The value at {} is out of bounds", idx)
    }

    for x in &vec { println!("{}", x)}
    while let Some(x) = vec.pop() {
        println!("{}", x);
    }

}