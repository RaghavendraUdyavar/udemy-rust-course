#![allow(dead_code)]

mod structs;
mod enums;
mod unions;
mod options;
mod arrays;
mod vectors;
mod slices;
mod strings;
mod tuples;
mod patternmatching;

pub fn call_createstructure(){
    structs::createstructure();
}

pub fn call_createenums() {
    enums::createenums();
}

pub fn call_createunions() {
    unions::createunions();
}

pub fn call_createoptions() {
    options::createoptions();
}

pub fn call_createarrays() {
    arrays::createarrays();
}

pub fn call_createvectors() {
    vectors::createvectors();
}

pub fn call_createslices() {
    slices::slices();
}

pub fn call_createstrings() {
    strings::createstrings();
}

pub fn call_createtuples() {
    tuples::createtuples();
}

pub fn call_patternmatching() {
    patternmatching::pattern_matching();
}