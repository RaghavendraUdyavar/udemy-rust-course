pub fn pattern_matching() {
    for x in 0..13 {
        println!("{} : I have {} oranges", x, how_many(x));
    }
}

fn how_many(x: i32) -> i32
{
    match x {
         0 => 0,
         _ => 1,
     }
}
